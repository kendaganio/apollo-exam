import React from "react";
import "./App.css";

import Controls from "./components/Controls";
import Player from "./components/Player";
import Transcript from "./components/Transcript";

function App() {
  return (
    <div className="app">
      <Controls />
      <Player />
      <Transcript />
    </div>
  );
}

export default App;
