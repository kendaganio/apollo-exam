import React from "react";
import ReactDOM from "react-dom";

import { PlayerProvider } from "./contexts/PlayerContext";
import App from "./App";

import "./index.css";

ReactDOM.render(
  <React.StrictMode>
    <PlayerProvider>
      <App />
    </PlayerProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
