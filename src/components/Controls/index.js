import React from "react";
import { usePlayerState } from "../../contexts/PlayerContext";

import rotateLeft from "./rotate-left.svg";
import rotateRight from "./rotate-right.svg";
import "./style.css";

export default function Controls(props) {
  const { status, playPause } = usePlayerState();

  if (status === "loading") {
    return "loading...";
  }

  return (
    <div className="controls">
      <div className="left">
        <div className="controls--player">
          <img src={rotateLeft} />
          <button className="controls--playPause" onClick={() => playPause()}>
            >||
          </button>
          <img src={rotateRight} />
        </div>
        <button className="controls--playbackSpeed">1.0x</button>
      </div>
      <div className="right"></div>
    </div>
  );
}
