import React, { createRef } from "react";
import ReactAudioPlayer from "react-audio-player";

import Waveform from "./Waveform";

import { usePlayerState } from "../../contexts/PlayerContext";
import audio from "../../test.wav";
import "./style.css";

export default function Player(props) {
  const { setCurrentTime, setPlayerState, audioRef } = usePlayerState();

  return (
    <div className="player">
      <Waveform />
      <ReactAudioPlayer
        ref={audioRef}
        src={audio}
        onCanPlay={() =>
          setPlayerState((state) => ({
            ...state,
            status: "paused",
            duration: audioRef.current.audioEl.current.duration,
          }))
        }
        listenInterval={10}
        onListen={setCurrentTime}
      />
    </div>
  );
}
