import React from "react";
import Wave from "./Wave";

import { usePlayerState } from "../../contexts/PlayerContext";
import transcript from "../../transcript.json";

export default function Waveform(props) {
  const { word_timings } = transcript;
  const { status } = usePlayerState();

  if (status === "loading") {
    return "loading";
  }

  return (
    <div className="waves">
      {word_timings.map((words, index) => {
        const isProspect = index % 2 === 0;
        const startTime = parseFloat(words[0].startTime, 10);
        const endTime = parseFloat(words[words.length - 1].endTime, 10);
        return (
          <Wave
            key={`wave-${index}`}
            isProspect={isProspect}
            startTime={startTime}
            endTime={endTime}
          />
        );
      })}
    </div>
  );
}
