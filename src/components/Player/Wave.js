import React from "react";
import { usePlayerState } from "../../contexts/PlayerContext";

export default function Wave({ startTime, endTime, isProspect, ...props }) {
  const { duration, currentTime } = usePlayerState();
  const percentage = ((endTime - startTime) / duration) * 100;

  return (
    <div
      className="wave"
      style={{
        backgroundImage: `linear-gradient(90deg, white 50%, ${
          isProspect ? "#1a99f6" : "#8868e9"
        } 50%`,
        backgroundSize: "4px 4px",
        width: `${percentage}%`,
      }}
    ></div>
  );
}
