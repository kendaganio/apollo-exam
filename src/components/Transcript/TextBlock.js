import React from "react";
import Word from "./Word";

export default function TextBlock({
  text,
  words,
  currentTime,
  isProspect,
  ...props
}) {
  const startTime = parseFloat(words[0].startTime, 10);
  const endTime = parseFloat(words[words.length - 1].endTime, 10);

  const formatTime = (t) => {
    const m = ("" + Math.floor(t / 60)).padStart(2, "0");
    const s = ("" + parseInt(t % 60, 10)).padStart(2, 0);
    return `${m}:${s}`;
  };

  const isSpeaking = currentTime >= startTime && currentTime <= endTime;

  return (
    <div
      className={`textblock ${isProspect ? "prospect" : "you"} ${
        isSpeaking && "speaking"
      }`}
    >
      <span className="textblock--startTime">{formatTime(startTime)}</span>
      <div className="textblock--content">
        {words.map((w, index) => (
          <Word key={`word-${index}`} {...w} />
        ))}
      </div>
    </div>
  );
}
