import React from "react";

import { usePlayerState } from "../../contexts/PlayerContext";
import TextBlock from "./TextBlock";

import transcript from "../../transcript.json";
import "./style.css";

export default function Transcript(props) {
  const { currentTime } = usePlayerState();

  return (
    <div className="transcript">
      <div className="transcript--searchBox">
        <input
          type="text"
          placeholder="Search call transcript"
          className="search-input"
        />
      </div>
      <div className="transcript--text">
        {transcript.transcript_text.map((t, index) => (
          <TextBlock
            key={`textblock-${index}`}
            text={t}
            words={transcript.word_timings[index]}
            currentTime={currentTime}
            // not entirely sure if the real data shows
            // each person speaking a different transcript_text
            // entry so this is just an assumption that i need
            // to make :D
            isProspect={index % 2 === 0}
          />
        ))}
      </div>
    </div>
  );
}
