import React from "react";

import { usePlayerState } from "../../contexts/PlayerContext";

export default function Word({ startTime, endTime, word }) {
  const { currentTime, seekTo } = usePlayerState();
  const start = parseFloat(startTime, 10);
  const end = parseFloat(endTime, 10);

  const isCurrent = currentTime >= start && currentTime <= end;

  return (
    <span
      onClick={(e) => {
        seekTo(start);
      }}
    >
      <span className={`word ${isCurrent && "current"}`}>{word}</span>{" "}
    </span>
  );
}
