import React, { createContext, useContext, useState, createRef } from "react";

const PlayerContext = createContext({});

function PlayerProvider(props) {
  const [playerState, setPlayerState] = useState({
    status: "loading",
    currentTime: 0.0,
    duration: 0.0,
    audioRef: createRef(null),
  });

  const setCurrentTime = (currentTime) => {
    setPlayerState((state) => ({
      ...state,
      currentTime,
    }));
  };

  const playPause = () => {
    const el = playerState.audioRef.current.audioEl.current;
    el.paused ? el.play() : el.pause();
    setPlayerState((state) => ({
      ...state,
      status: el.paused ? "paused" : "playing",
    }));
  };

  const seekTo = (currentTime) => {
    const el = playerState.audioRef.current.audioEl.current;
    el.currentTime = currentTime;
    el.play();
    setPlayerState((state) => ({
      ...state,
      status: "playing",
      currentTime,
    }));
  };

  return (
    <PlayerContext.Provider
      value={{
        ...playerState,
        setCurrentTime,
        setPlayerState,
        seekTo,
        playPause,
      }}
      {...props}
    />
  );
}

const usePlayerState = () => useContext(PlayerContext);

export { PlayerProvider, usePlayerState };
